# Install

sudo coreos-installer install /dev/sdx --copy-network --ignition-url https://gitlab.com/viery/configs/-/raw/main/ignition/coreos.ign

# Change PW
sudo passwd core

# docker-compose
sudo rpm-ostree install docker-compose
